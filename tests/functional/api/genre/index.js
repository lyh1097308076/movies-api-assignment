import chai from "chai";
import request from "supertest";
import api from "../../../../index";

const expect = chai.expect;
let genres;

describe("Genres endpoint", () => {
    describe("GET /api/movies/tmdb/genres ", () => {
        before(async() => {
            await request('https://api.themoviedb.org')
                .get(`/3/genre/movie/list?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US`)
                .then((res) => {
                    genres = res.body
                })
        })
        it("should return upcoming movies and a status 200", () => {
            return request(api)
                .get("/api/movies/tmdb/genres")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(genres)
                });
        });
    });
})