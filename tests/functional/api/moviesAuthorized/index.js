import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import User from "../../../../api/users/userModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";

const expect = chai.expect;
let db;
let movie;

describe("Authorized Movies endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async() => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });

    beforeEach(async() => {
        try {
            await Movie.deleteMany();
            await Movie.collection.insertMany(movies);
            await User.deleteMany();
            // Register two users
            await request(api).post("/api/users?action=register").send({
                username: "user1",
                password: "test1",
            });
        } catch (err) {
            console.error(`failed to Load user Data: ${err}`);
        }
    });
    afterEach(() => {
        api.close(); // Release PORT 8080
    });
    describe("GET /api/movies-auth/tmdb/movie/:id ", () => {
        before(() => {
            request('https://api.themoviedb.org')
                .get(`/3/movie/411?api_key=${process.env.REACT_APP_TMDB_KEY}`)
                .end((err, res) => {
                    movie = res.body
                })
        })
        it("should return movie details and a status 200", () => {
            return request(api)
                .get("/api/movies-auth/tmdb/movie/411")
                .set('Authorization', `BEARER ${process.env.Authorization}`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movie)
                });
        });
    });
    describe("GET /api/movies-auth/tmdb/movie/:id/images ", () => {
        before(() => {
            request('https://api.themoviedb.org')
                .get(`/3/movie/411/images?api_key=${process.env.REACT_APP_TMDB_KEY}`)
                .end((err, res) => {
                    movie = res.body
                })
        })
        it("should return movie images and a status 200", () => {
            return request(api)
                .get("/api/movies-auth/tmdb/movie/411/images")
                .set('Authorization', `BEARER ${process.env.Authorization}`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movie)
                });
        });
    });
    describe("GET /api/movies-auth/tmdb/movie/:id/reviews ", () => {
        before(() => {
            request('https://api.themoviedb.org')
                .get(`/3/movie/411/reviews?api_key=${process.env.REACT_APP_TMDB_KEY}`)
                .end((err, res) => {
                    movie = res.body
                })
        })
        it("should return movie reviews and a status 200", () => {
            return request(api)
                .get("/api/movies-auth/tmdb/movie/411/reviews")
                .set('Authorization', `BEARER ${process.env.Authorization}`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("array");
                    expect(res.body).to.eql(movie.results)
                });
        });
    });
    describe("GET /api/movies-auth/tmdb/company/:id ", () => {
        before(() => {
            request('https://api.themoviedb.org')
                .get(`/3/company/5?api_key=${process.env.REACT_APP_TMDB_KEY}`)
                .end((err, res) => {
                    movie = res.body
                })
        })
        it("should return company details and a status 200", () => {
            return request(api)
                .get("/api/movies-auth/tmdb/company/5")
                .set('Authorization', `BEARER ${process.env.Authorization}`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movie)
                });
        });
    });
    describe("GET /api/movies-auth/tmdb/company/:id/images ", () => {
        before(() => {
            request('https://api.themoviedb.org')
                .get(`/3/company/5/images?api_key=${process.env.REACT_APP_TMDB_KEY}`)
                .end((err, res) => {
                    movie = res.body
                })
        })
        it("should return company images and a status 200", () => {
            return request(api)
                .get("/api/movies-auth/tmdb/company/5/images")
                .set('Authorization', `BEARER ${process.env.Authorization}`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movie)
                });
        });
    });
    describe("GET /api/movies-auth/tmdb/credits/:id ", () => {
        before(() => {
            request('https://api.themoviedb.org')
                .get(`/3/movie/411/credits?api_key=${process.env.REACT_APP_TMDB_KEY}`)
                .end((err, res) => {
                    movie = res.body
                })
        })
        it("should return movie credits and a status 200", () => {
            return request(api)
                .get("/api/movies-auth/tmdb/credits/411")
                .set('Authorization', `BEARER ${process.env.Authorization}`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movie)
                });
        });
    });
    describe("GET /api/movies-auth/tmdb/similar/:id ", () => {
        before(() => {
            request('https://api.themoviedb.org')
                .get(`/3/movie/411/similar?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1`)
                .end((err, res) => {
                    movie = res.body
                })
        })
        it("should return similar movies and a status 200", () => {
            return request(api)
                .get("/api/movies-auth/tmdb/similar/411")
                .set('Authorization', `BEARER ${process.env.Authorization}`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movie)
                })
        });
    });
    describe("GET /api/movies-auth/tmdb/recommand/:id ", () => {
        before(() => {
            request('https://api.themoviedb.org')
                .get(`/3/movie/411/recommendations?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1`)
                .end((err, res) => {
                    movie = res.body
                })
        })
        it("should return recommand movies and a status 200", () => {
            return request(api)
                .get("/api/movies-auth/tmdb/recommand/411")
                .set('Authorization', `BEARER ${process.env.Authorization}`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movie)
                })
        });
    });
});