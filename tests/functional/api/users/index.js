import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;

describe("Users endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async() => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });
    beforeEach(async() => {
        try {
            await User.deleteMany();
            // Register two users
            await request(api).post("/api/users?action=register").send({
                username: "user1",
                password: "test1",
            });
            await request(api).post("/api/users?action=register").send({
                username: "user2",
                password: "test2",
            });
        } catch (err) {
            console.error(`failed to Load user test Data: ${err}`);
        }
    });
    afterEach(() => {
        api.close();
    });

    describe("POST /api/users ", () => {
        describe("if do not pass any username and password", () => {
            it("should return a status 401", () => {
                return request(api)
                    .post("/api/users")
                    .expect(401)
            })
        })
        describe("For an authenticate action", () => {
            describe("if username is wrong", () => {
                it("should return a 401 status", () => {
                    return request(api)
                        .post("/api/users?action=authenticate")
                        .send({
                            username: "user3",
                            password: "test1",
                        })
                        .expect(401)
                })
            })
            describe("if password is wrong", () => {
                it("should return a 401 status", () => {
                    return request(api)
                        .post("/api/users?action=authenticate")
                        .send({
                            username: "user1",
                            password: "tes",
                        })
                        .expect(401)
                })
            })
        });
    });
});