import chai from "chai";
import request from "supertest";
import api from "../../../../index";

const expect = chai.expect;
let movies;

describe("Movie List endpoint", () => {
    describe("GET /api/movies/tmdb/upcoming ", () => {
        before(async() => {
            await request('https://api.themoviedb.org')
                .get(`/3/movie/upcoming?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1`)
                .then((res) => {
                    movies = res.body
                })
        })
        it("should return upcoming movies and a status 200", () => {
            return request(api)
                .get("/api/movies/tmdb/upcoming?page=1")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movies)
                });
        });
    });
    describe("GET /api/movies/tmdb/now_playing ", () => {
        before(async() => {
            await request('https://api.themoviedb.org')
                .get(`/3/movie/now_playing?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1`)
                .then((res) => {
                    movies = res.body
                })
        })
        it("should return now_playing movies and a status 200", () => {
            return request(api)
                .get("/api/movies/tmdb/now_playing?page=1")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movies)
                });
        });
    });
    describe("GET /api/movies/tmdb/discover ", () => {
        before(async() => {
            await request('https://api.themoviedb.org')
                .get(`/3/discover/movie?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&include_adult=false&include_video=false&page=1`)
                .then((res) => {
                    movies = res.body
                })
        })
        it("should return discover movies and a status 200", () => {
            return request(api)
                .get("/api/movies/tmdb/discover?page=1")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movies)
                });
        });
    });
    describe("GET /api/movies/tmdb/popular ", () => {
        before(async() => {
            await request('https://api.themoviedb.org')
                .get(`/3/movie/popular?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1`)
                .then((res) => {
                    movies = res.body
                })
        })
        it("should return popular movies and a status 200", () => {
            return request(api)
                .get("/api/movies/tmdb/popular?page=1")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movies)
                });
        });
    });
    describe("GET /api/movies/tmdb/top_rated ", () => {
        before(async() => {
            await request('https://api.themoviedb.org')
                .get(`/3/tv/top_rated?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1`)
                .then((res) => {
                    movies = res.body
                })
        })
        it("should return top_rated tv and a status 200", () => {
            return request(api)
                .get("/api/movies/tmdb/top_rated?page=1")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("object");
                    expect(res.body).to.eql(movies)
                })
        });
    });
});