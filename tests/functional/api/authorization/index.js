import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import User from "../../../../api/users/userModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";

const expect = chai.expect;
let db;
let movie;

describe("Authorized Movies endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async() => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });

    beforeEach(async() => {
        try {
            await Movie.deleteMany();
            await Movie.collection.insertMany(movies);
            await User.deleteMany();
            // Register two users
            await request(api).post("/api/users?action=register").send({
                username: "user1",
                password: "test1",
            });
        } catch (err) {
            console.error(`failed to Load user Data: ${err}`);
        }
    });
    afterEach(() => {
        api.close(); // Release PORT 8080
    });
    describe("GET /api/movies-auth/tmdb/movie/:id ", () => {
        describe("reques with authorization", () => {
            before(() => {
                request('https://api.themoviedb.org')
                    .get(`/3/movie/411?api_key=${process.env.REACT_APP_TMDB_KEY}`)
                    .end((err, res) => {
                        movie = res.body
                    })
            })
            it("should return movie details and a status 200", () => {
                return request(api)
                    .get("/api/movies-auth/tmdb/movie/411")
                    .set('Authorization', `BEARER ${process.env.Authorization}`)
                    .set("Accept", "application/json")
                    .expect(200)
                    .then((res) => {
                        expect(res.body).to.be.a("object");
                        expect(res.body).to.eql(movie)
                    });
            });
        })
        describe("request without authorization", () => {
            before(() => {
                request('https://api.themoviedb.org')
                    .get(`/3/movie/411?api_key=${process.env.REACT_APP_TMDB_KEY}`)
                    .end((err, res) => {
                        movie = res.body
                    })
            })
            it("should a status 401", () => {
                return request(api)
                    .get("/api/movies-auth/tmdb/movie/411")
                    .set("Accept", "application/json")
                    .expect(401)
            });
        })
    });
})