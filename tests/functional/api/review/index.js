import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Review from "../../../../api/reviews/reviewModel";
import api from "../../../../index";

const expect = chai.expect;
let db;

describe("Review endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async() => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });
    beforeEach(async() => {
        try {
            await Review.deleteMany();
        } catch (err) {
            console.error(err);
        }
    });
    afterEach(() => {
        api.close();
    });
    describe("GET /api/reviews", () => {
        describe("there are some reviews", () => {
            beforeEach(async() => {
                await request(api)
                    .post("/api/reviews")
                    .send({
                        review: {
                            author: "LuMing",
                            review: "This is a good movie.",
                            rating: 3,
                            movieId: "123456"
                        }
                    })
                await request(api)
                    .post("/api/reviews")
                    .send({
                        review: {
                            author: "Lanrico",
                            review: "This is a bad movie.",
                            rating: 1,
                            movieId: "415236"
                        }
                    })
            })
            it("should return all the reviews and a status 200", (done) => {
                request(api)
                    .get("/api/reviews")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body).to.be.a("array")
                        expect(res.body.length).to.equal(2)
                        done()
                    })
            })
        })
        describe("there are no reviews", () => {
            it("should return a status 404", () => {
                return request(api)
                    .get("/api/reviews")
                    .expect(404)
            })
        })

    })
    describe("POST /api/reviews", () => {
        describe("input a correct review", () => {
            it("should return a status 200", () => {
                return request(api)
                    .post("/api/reviews")
                    .send({
                        review: {
                            author: "LuMing",
                            review: "This is a good movie.",
                            rating: 3,
                            movieId: "123456"
                        }
                    })
                    .expect(200)
            })
        })
        describe("input a wrong review", () => {
            it("should return a status 401", () => {
                return request(api)
                    .post("/api/reviews")
                    .send({
                        review: {
                            review: "This is a good movie.",
                            rating: 3
                        }
                    })
                    .expect(401)
            })
        })
    })
})