import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;

describe("Favourite endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async() => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });
    beforeEach(async() => {
        try {
            await User.deleteMany();
            // Register two users
            await request(api).post("/api/users?action=register").send({
                username: "user1",
                password: "test1",
            });
        } catch (err) {
            console.error(`failed to Load user test Data: ${err}`);
        }
    });
    afterEach(() => {
        api.close();
    });
    describe("POST /api/users/:userName/favourites ", () => {
        describe("input a new favourite movie", () => {
            it("should add movie id to database and a status 200", async() => {
                await request(api)
                    .post("/api/users/user1/favourites")
                    .send({
                        id: "321453"
                    })
                    .expect(201)
                const user = await User.findByUserName('user1').populate('favourites');
                expect(user.favourites).to.include("321453")
            });
        })
        describe("input a duplicate movie", () => {
            beforeEach(async() => {
                await request(api)
                    .post("/api/users/user1/favourites")
                    .send({
                        id: "321453"
                    })
            })
            it("should return a status 404", () => {
                return request(api)
                    .post("/api/users/user1/favourites")
                    .send({
                        id: "321453"
                    })
                    .expect(404)
            })
        })
        describe("input an unexisted user", () => {
            it("should return a status 401", () => {
                return request(api)
                    .post("/api/users/user3/favourites")
                    .send({
                        id: "321453"
                    })
                    .expect(404)
            })
        })
    });
    describe("GET /api/users/:userName/favourites ", () => {
        describe("input an existed user", () => {
            beforeEach(async() => {
                await request(api)
                    .post("/api/users/user1/favourites")
                    .send({
                        id: "654321"
                    })
                await request(api)
                    .post("/api/users/user1/favourites")
                    .send({
                        id: "123456"
                    })
            })
            it("should return the favourite movies and a status 200", async() => {
                await request(api)
                    .get("/api/users/user1/favourites")
                    .expect(200)
                const user = await User.findByUserName('user1').populate('favourites');
                expect(user.favourites).to.be.a("array")
                expect(user.favourites.length).to.equal(2)
                expect(user.favourites).to.have.members(["123456", "654321"])
            })
        })
        describe("input and unexisted user", () => {
            it("should return a status 401", () => {
                return request(api)
                    .get("/api/users/user3/favourites")
                    .expect(404)
            })
        })
    })
    describe("POST /api/users/:userName/favourites/delete", () => {
        beforeEach(async() => {
            await request(api)
                .post("/api/users/user1/favourites")
                .send({
                    id: "654321"
                })
            await request(api)
                .post("/api/users/user1/favourites")
                .send({
                    id: "123456"
                })
            await request(api)
                .post("/api/users/user1/favourites")
                .send({
                    id: "142536"
                })
        })
        describe("input correct username and movie id", () => {
            it("should return a status 201", () => {
                return request(api)
                    .post("/api/users/user1/favourites/delete")
                    .send({
                        id: "142536"
                    })
                    .expect(201)
            })
        })
        describe("input an unexisted user and a correct movie id", () => {
            it("should return a status 404", () => {
                return request(api)
                    .post("/api/users/user3/favourites/delete")
                    .send({
                        id: "142536"
                    })
                    .expect(404)
            })
        })
        describe("input a correct user and an unexisted movie id", () => {
            it("should return a status 401", () => {
                return request(api)
                    .post("/api/users/user1/favourites/delete")
                    .send({
                        id: "321654"
                    })
                    .expect(401)
            })
        })
        describe("input unexisted username and movie id", () => {
            it("should return a status 404", () => {
                return request(api)
                    .post("/api/users/user3/favourites/delete")
                    .send({
                        id: "321654"
                    })
                    .expect(404)
            })
        })
    })
});