# Assignment 2 - Agile Software Practice.
​
Name: Yuanhao Luo
​
## API endpoints.

+ GET /api/users - Get all users.
+ POST /api/users?action=action - Get authorization token or register account. set action to register or login. 
+ GET /api/movies - Get movies in seed data.
+ GET /api/movies/:id - Get movie details in seed data. 
+ GET /api/movies-auth/tmdb/movie/:id (Auth) - Get movie details from TMDB.
+ GET /api/movies-auth/tmdb/movie/:id/images (Auth) - Get movie images from TMDB.
+ GET /api/movies-auth/tmdb/movie/:id/reviews (Auth) - Get movie reviews from TMDB.
+ GET /api/movies-auth/tmdb/company/:id (Auth) - Get comapany details from TMDB.
+ GET /api/movies-auth/tmdb/company/:id/images (Auth) - Get company images from TMDB.
+ GET /api/movies-auth/tmdb/credits/:id (Auth) - Get movie credits from TMDB.
+ GET /api/movies-auth/tmdb/similar/:id (Auth) - Get similar movies from TMDB.
+ GET /api/movies-auth/tmdb/recommand/:id (Auth) - Get recommand movies from TMDB.
+ GET /api/movies/tmdb/upcoming?page=p - Get upcoming movies in page p from TMDB.
+ GET /api/movies/tmdb/now_playing?page=p - Get now playing movies in page p from TMDB.
+ GET /api/movies/tmdb/discover?page=p - Get discover movies in page p from TMDB.
+ GET /api/movies/tmdb/popular?page=p - Get popular movies in page p from TMDB.
+ GET /api/movies/tmdb/top_rated?page=p - Get top rated tvs in page p from TMDB.
+ POST /api/users/:userName/favourites - Add a favourite movie to a user.
+ GET /api/users/:userName/favourites - Get all the favourite movies of a user.
+ POST /api/users/:userName/favourites/delete - Delete a user's favourite movie.
+ GET /api/movies/tmdb/genres - Get genres from TMDB.
​
## Test cases.
​
~~~
  Users endpoint
    POST /api/users
      if do not pass any username and password
database connected to movies on ac-tawevae-shard-00-00.aueovym.mongodb.net
        √ should return a status 401
      For an authenticate action
        if username is wrong
          √ should return a 401 status
        if password is wrong
          √ should return a 401 status (195ms)

  Movies endpoint
    GET /api/movies
      √ should return 20 movies and a status 200
    GET /api/movies/:id
      when the id is valid
        √ should return the matching movie
      when the id is invalid
        √ should return the NOT found message

  Authorized Movies endpoint
    GET /api/movies-auth/tmdb/movie/:id
      √ should return movie details and a status 200 (109ms)
    GET /api/movies-auth/tmdb/movie/:id/images
      √ should return movie images and a status 200 (110ms)
    GET /api/movies-auth/tmdb/movie/:id/reviews
      √ should return movie reviews and a status 200 (101ms)
    GET /api/movies-auth/tmdb/company/:id
      √ should return company details and a status 200 (97ms)
    GET /api/movies-auth/tmdb/company/:id/images
      √ should return company images and a status 200 (138ms)
    GET /api/movies-auth/tmdb/credits/:id
      √ should return movie credits and a status 200 (106ms)
    GET /api/movies-auth/tmdb/similar/:id
      √ should return similar movies and a status 200 (1110ms)
    GET /api/movies-auth/tmdb/recommand/:id
      √ should return recommand movies and a status 200 (112ms)

  Favourite endpoint
    POST /api/users/:userName/favourites
      input a new favourite movie
        √ should add movie id to database and a status 200 (72ms)
      input a duplicate movie
        √ should return a status 404
      input an unexisted user
        √ should return a status 401
    GET /api/users/:userName/favourites
      input an existed user
        √ should return the favourite movies and a status 200 (46ms)
      input and unexisted user
        √ should return a status 401
    POST /api/users/:userName/favourites/delete
      input correct username and movie id
        √ should return a status 201 (52ms)
      input an unexisted user and a correct movie id
        √ should return a status 404
      input a correct user and an unexisted movie id
        √ should return a status 401
      input unexisted username and movie id
        √ should return a status 404

  Movie List endpoint
    GET /api/movies/tmdb/upcoming
      √ should return upcoming movies and a status 200 (263ms)
    GET /api/movies/tmdb/now_playing
      √ should return now_playing movies and a status 200 (90ms)
    GET /api/movies/tmdb/discover
      √ should return discover movies and a status 200 (84ms)
    GET /api/movies/tmdb/popular
      √ should return popular movies and a status 200 (88ms)
    GET /api/movies/tmdb/top_rated
      √ should return top_rated tv and a status 200 (84ms)

  Review endpoint
    GET /api/reviews
      there are some reviews
        √ should return all the reviews and a status 200
      there are no reviews
        √ should return a status 404
    POST /api/reviews
      input a correct review
        √ should return a status 200
      input a wrong review
        √ should return a status 401

  Genres endpoint
    GET /api/movies/tmdb/genres
      √ should return upcoming movies and a status 200 (82ms)

  Authorized Movies endpoint
    GET /api/movies-auth/tmdb/movie/:id
      reques with authorization
        √ should return movie details and a status 200 (103ms)
      request without authorization
        √ should a status 401


  35 passing (14s)
~~~

## Independent Learning (if relevant)

Coveralls: https://coveralls.io/gitlab/Yuanhao-Luo/movies-api-assignment
​Heroku Staing: https://movie-api-assignment.herokuapp.com/
Heroku Production: https://movie-api-deploy-yl.herokuapp.com/
GitLab: https://gitlab.com/Yuanhao-Luo/movies-api-assignment