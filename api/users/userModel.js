import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    favourites: [{ type: String }]
});

UserSchema.statics.findByUserName = function(username) {
    return this.findOne({ username: username });
};

UserSchema.methods.comparePassword = function(passw, callback) {
    bcrypt.compare(passw, this.password, (err, isMatch) => {
        callback(null, isMatch);
    });
};

UserSchema.pre('save', function(next) {
    const user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, null, (err, hash) => {
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

export default mongoose.model('User', UserSchema);