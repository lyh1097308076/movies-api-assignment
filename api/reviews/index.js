import express from 'express';
import jwt from 'jsonwebtoken';
import Review from './reviewModel';
import movieModel from '../movies/movieModel';
import asyncHandler from 'express-async-handler';
import e from 'express';


const router = express.Router(); // eslint-disable-line

router.post('/', asyncHandler(async(req, res) => {
    const re = req.body.review
    if (re.author != undefined && re.review != undefined && re.rating != undefined && re.movieId != undefined) {
        await Review.create(req.body.review);
        res.status(200).json(req.body);
    } else {
        res.status(401).json({ code: 201, msg: 'Wrong Review' });
    }
}));

router.get('/', asyncHandler(async(req, res) => {
    const reviews = await Review.find()
    if (reviews.length > 0) {
        res.status(200).json(reviews);
    } else {
        res.status(404).json({ code: 404, msg: 'Reviews not found' });
    }

}))


export default router;