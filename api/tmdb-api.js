import fetch from 'node-fetch';

export const getUpcoming = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=${args}`
    ).then((response) => {
        return response.json();
    })
};

export const getMovies = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/discover/movie?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&include_adult=false&include_video=false&page=${args}`
    ).then((response) => {
        return response.json();
    })
};

export const getMovie = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/movie/${args}?api_key=${process.env.REACT_APP_TMDB_KEY}`
    ).then((response) => {
        return response.json();
    })
};

export const getGenres = async() => {
    return fetch(
        "https://api.themoviedb.org/3/genre/movie/list?api_key=" +
        process.env.REACT_APP_TMDB_KEY +
        "&language=en-US"
    ).then((response) => {
        return response.json();
    })
};

export const getMovieImages = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/movie/${args}/images?api_key=${process.env.REACT_APP_TMDB_KEY}`
    ).then((response) => {
        return response.json();
    })
};

export const getMovieReviews = (id) => {
    return fetch(
            `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=${process.env.REACT_APP_TMDB_KEY}`
        )
        .then((res) => res.json())
        .then((json) => {
            return json.results;
        });
};

export const getCompany = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/company/${args}?api_key=${process.env.REACT_APP_TMDB_KEY}`
    ).then((response) => {
        return response.json();
    })
};

export const getCompanyImages = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/company/${args}/images?api_key=${process.env.REACT_APP_TMDB_KEY}`
    ).then((response) => {
        return response.json();
    })
};


export const getCredits = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/movie/${args}/credits?api_key=${process.env.REACT_APP_TMDB_KEY}`
    ).then((response) => {
        return response.json();
    })
};

export const getSimilar = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/movie/${args}/similar?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1`
    ).then((response) => {
        return response.json();
    })
};

export const getNowPlaying = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/movie/now_playing?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=${args}`
    ).then((response) => {
        return response.json();
    })
};

export const getPopular = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/movie/popular?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=${args}`
    ).then((response) => {
        return response.json();
    })
};

export const getTopRatedTV = (args) => {
    return fetch(
        `https://api.themoviedb.org/3/tv/top_rated?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=${args}`
    ).then((response) => {
        return response.json();
    })
};

export const getRecommand = (id) => {
    return fetch(
        `https://api.themoviedb.org/3/movie/${id}/recommendations?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1`
    ).then((response) => {
        return response.json();
    })
};